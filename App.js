
import { StyleSheet, View } from 'react-native';
import Fetch from './src/Fetch';
import Add from './src/Add';
import UploadMediaFile from './src/UploadMediaFile';

export default function App() {
  return (
    <View style={styles.container}>
        {/* <Fetch></Fetch> */}
        {/* <Add></Add> */}
        <UploadMediaFile></UploadMediaFile>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    flex: 1,
  },
});
