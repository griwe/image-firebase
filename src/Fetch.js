import { View, Text, FlatList, StyleSheet, Pressable } from 'react-native'
import React, {useState, useEffect} from 'react'
import { firebase } from '../config';

const Fetch = () => {

    const [users, setUsers] = useState([]);
    const todoRef = firebase.firestore().collection('todos');

    useEffect(async () => {
        todoRef.onSnapshot(
            querySnapshot => {
                const users = []
                querySnapshot.forEach((doc) => {
                    const {heading, text} = doc.data()
                    users.push({
                        id: doc.id,
                        heading,
                        text,
                    })
                })
                setUsers(users)
            }
        )
    }, [])

    return (
        <View>
            <FlatList 
                data={users}
                numColumns={1}
                renderItem={({item}) => (
                    <Pressable >
                        <View >
                            <Text>{item.heading}</Text>
                            <Text>{item.text}</Text>
                        </View>
                    </Pressable>
                )}
            />
                
        </View>
    )
}

export default Fetch